## Функциональные требования сервиса
1. Сервис должен предоставлять возможность создания сокращенных URL на основе длинных;
2. При переходе по сокращенной ссылке пользователь должен быть перенаправлен на исходный URL;
3. Сервис должен проверять уникальность сокращенных ссылок;
4. Сервис должен предоставлять возможность редактирования ранее созданных сокращенных ссылок;
5. Сервис должен добавлять в кеш наиболее популярные сокращенные ссылки;
6. Сервис должен обеспечивать хранение данных без ограничений по времени хранения;
7. Сервис должен предоставлять API для доступа к функционалу создания/редактирование/удаления сокращенных ссылок. 

## Нефункциональные требования сервиса
1. Сервис должен обеспечивать доступность в течение всего времени работы;
2. Сервис должен быть масштабируемым для обеспечения возможноти обработки большого количества запросов;
3. Сервис должен работать без перебоев;
4. Сервис должен обеспечивать быстрое создание сокращенных ссылок и перенаправление по ним;
5. Сервис должен обеспечивать безопасность пользовательских данных.

## Схема базы данных сервиса
```plantuml
skinparam linetype ortho

entity ShortUrl {
  * id : VARCHAR(10)
  --
  * source_url : TEXT
}

entity User {
  * id : uuid <<generated>>
  --
  * login : TEXT <<unique>>
  * password : TEXT
}

entity Role {
  * id : INTEGER <<generated>>
  --
  * name : TEXT <<unique>>
}

entity User_Role {
  * user_id : uuid <<FK>>
  * role_id : INTEGER <<FK>>
  --
}

entity User_ShortUrl {
  * user_id : uuid <<FK>>
  * short_url_id : VARCHAR(10) <<FK>>
  --
}

User     ||--|{ User_Role
Role     ||--|{ User_Role

User     ||--|{ User_ShortUrl
ShortUrl ||--|{ User_ShortUrl
```

## Верхнеуровневые диаграммы взаимодействия пользователя с сервисом
### Получение всех сокращенных ссылок
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : get all short urls request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> db : select all short urls
    db --> service : all short urls
    service --> client : all short urls
  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Получение сокращенной ссылки по идентификатору
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : get short url by id request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> db : select short url by id

    alt #63b4cf Short url found
      db --> service : short url
      service --> client : short url
    else #Pink Short url not found
      db --> service : short url not found
      service --> client : short url not found
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Создание сокращенной ссылки
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : create short url request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> service : authentication check

    alt #63b4cf Authentication passed
      service -> service : payload check

      alt #3691b0 Payload check passed
        service -> db : insert short url
        db --> service : short url
        service -> db : insert userShortUrl entity
        db --> service : inserted
        service --> client : short url
      else #Pink Payload check failed
        service --> client : payload error
      end

    else #Pink Authentication error
      service --> client : authentication error
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Изменение сокращенной ссылки
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : update short url request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> service : authentication check

    alt #63b4cf Authentication passed
      service -> service : payload check

      alt #3691b0 Payload check passed
        service -> db : update short url

        alt #30819c Short url found
          db --> service : updated
          service --> client : no content
        else #Pink Short url not found
          db --> service : short url not found
          service --> client : short url not found
        end

      else #Pink Payload check failed
        service --> client : payload error
      end

    else #Pink Authentication error
      service --> client : authentication error
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Удаление сокращенной ссылки
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : remove short url request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> service : authentication check

    alt #63b4cf Authentication passed
      service -> db : delete short url

      alt #3691b0 Short url found
        db --> service : deleted
        service --> client : no content
      else #Pink Short url not found
        db --> service : short url not found
        service --> client : short url not found
      end

    else #Pink Authentication error
      service --> client : authentication error
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Получение пользователя по идентификатору
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : get user by id request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> service : authentication check

    alt #63b4cf Authentication passed
      service -> db : select user by id

      alt #3691b0 User found
        db --> service : user
        service -> db : select roles by user id
        db --> service : user roles
        service --> client : user with roles
      else #Pink User not found
        db --> service : user not found
        service --> client : user not found
      end

    else #Pink Authentication error
      service --> client : authentication error
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Создание пользователя
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : create user request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> service : authentication check

    alt #63b4cf Authentication passed
      service -> service : payload check

      alt #3691b0 Payload check passed
        service -> db : insert user
        db --> service : user
        service -> db : insert userRole entity
        db --> service : inserted
        service --> client : user with roles
      else #Pink Payload check failed
        service --> client : payload error
      end

    else #Pink Authentication error
      service --> client : authentication error
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Изменение пользователя
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : update user request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> service : authentication check

    alt #63b4cf Authentication passed
      service -> service : payload check

      alt #3691b0 Payload check passed
        service -> db : update user

        alt #30819c User found
          db --> service : updated
          service -> db : update userRoles
          db --> service : updated
          service --> client : no content
        else #Pink User not found
          db --> service : user not found
          service --> client : user not found
        end

      else #Pink Payload check failed
        service --> client : payload error
      end

    else #Pink Authentication error
      service --> client : authentication error
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Удаление пользователя
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : remove short url request
activate service
service -> db : select user by login
activate db

alt #c4e3ed Identification passed
  db --> service : user
  service -> service : authorization check

  alt #9dd0e1 Authorization passed
    service -> service : authentication check

    alt #63b4cf Authentication passed
      service -> db : delete user

      alt #3691b0 User found
        db --> service : deleted
        service --> client : no content
      else #Pink User not found
        db --> service : user not found
        service --> client : user not found
      end

    else #Pink Authentication error
      service --> client : authentication error
    end

  else #Pink Authorization error
    service --> client : authorization error
  end

else #Pink Identification error
  db --> service : user not found
  deactivate db
  service --> client : identification error
  deactivate service
end

deactivate client
```

### Перенаправление по сокращенной ссылке
```plantuml
actor Client as client
participant UrlShortener as service
database Database as db

activate client
client -> service : redirect request
activate service
service -> db : select short url by id
activate db

alt #c4e3ed Short url found
  db --> service : short url
  service --> client : redirect
else #Pink Short url not found
  db --> service : short url not found
  deactivate db
  service --> client : short url not found
  deactivate service
end

deactivate client
```

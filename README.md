# Академия Бэкенда. Scala
### Домашнее задание № 1
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_1.md)
### Домашнее задание № 2
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_2.md)
### Домашнее задание № 3
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_3.md)
### Домашнее задание № 4
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_4.md)
### Домашнее задание № 5
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_5.md)
### Домашнее задание № 6
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_6.md)
### Домашнее задание № 7
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_7.md)
### Домашнее задание № 8
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_8.md)
### Домашнее задание № 9
[Условия](https://gitlab.com/backend-academy-scala-2022-2023/homework-spring-2023/-/blob/main/homework_9.md)
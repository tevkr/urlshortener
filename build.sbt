val http4sVersion              = "0.23.18"
val scalaTestVersion           = "3.2.14"
val tapirVersion               = "1.2.8"
val doobieVersion              = "1.0.0-RC2"
val quillVersion               = "4.6.0"
val flywayVersion              = "9.15.2"
val testcontainersScalaVersion = "0.40.12"
val pureconfigVersion          = "0.17.2"
val logbackVersion             = "1.3.3"
val scalaLoggingVersion        = "3.9.4"
val tofuVersion                = "0.11.1"
val promVersion                = "0.16.0"
val enumeratumVersion          = "1.7.2"

lazy val serverSettings = Seq(
  libraryDependencies ++= Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server"      % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe"         % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle"  % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-prometheus-metrics" % tapirVersion,
    "org.http4s"                  %% "http4s-ember-server"      % http4sVersion,
    "ch.qos.logback"               % "logback-classic"          % logbackVersion,
    "com.typesafe.scala-logging"  %% "scala-logging"            % scalaLoggingVersion,
    "tf.tofu"                     %% "tofu-core-ce3"            % tofuVersion,
    "tf.tofu"                     %% "tofu-logging-derivation"  % tofuVersion,
    "tf.tofu"                     %% "tofu-logging-layout"      % tofuVersion,
    "tf.tofu"                     %% "tofu-logging-structured"  % tofuVersion,
    "io.prometheus"                % "simpleclient_common"      % promVersion,
    "io.prometheus"                % "simpleclient_hotspot"     % promVersion
  )
)
lazy val testSetting = Seq(
  libraryDependencies ++= Seq(
    "com.dimafeng"                %% "testcontainers-scala-scalatest"  % testcontainersScalaVersion,
    "com.dimafeng"                %% "testcontainers-scala-postgresql" % testcontainersScalaVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-sttp-stub-server"          % tapirVersion     % Test,
    "org.scalatest"               %% "scalatest"                       % scalaTestVersion % Test
  )
)
lazy val dbSettings = Seq(
  libraryDependencies ++= Seq(
    "org.tpolecat" %% "doobie-core"      % doobieVersion,
    "org.tpolecat" %% "doobie-hikari"    % doobieVersion,
    "org.tpolecat" %% "doobie-postgres"  % doobieVersion,
    "org.tpolecat" %% "doobie-h2"        % doobieVersion,
    "org.tpolecat" %% "doobie-scalatest" % doobieVersion % Test,
    "org.flywaydb"  % "flyway-core"      % flywayVersion % Test,
    "io.getquill"  %% "quill-doobie"     % quillVersion
  )
)
lazy val utilsSettings = Seq(
  libraryDependencies ++= Seq(
    "com.beachape" %% "enumeratum" % enumeratumVersion,
    "org.mindrot"   % "jbcrypt"    % "0.4"
  )
)
lazy val configSettings = Seq(
  libraryDependencies ++= Seq(
    "com.github.pureconfig" %% "pureconfig"             % pureconfigVersion,
    "com.github.pureconfig" %% "pureconfig-cats-effect" % pureconfigVersion
  )
)
lazy val compilerPluginsSettings = Seq(
  libraryDependencies ++= Seq(compilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"))
)

lazy val root = (project in file("."))
  .settings(organization := "dev.yanovsky", name := "myproject", version := "0.0.1-SNAPSHOT", scalaVersion := "2.13.10")
  .settings(serverSettings, testSetting, dbSettings, configSettings, compilerPluginsSettings, utilsSettings)
  .settings(Compile / unmanagedResourceDirectories ++= Seq((ThisBuild / baseDirectory).value / "sql"))
  .enablePlugins(DockerPlugin, JavaAppPackaging)

addSbtPlugin("org.scalameta"  % "sbt-scalafmt"        % "2.5.0")
addSbtPlugin("org.scoverage"  % "sbt-scoverage"       % "2.0.6")
addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.16")

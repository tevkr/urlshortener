CREATE TABLE short_urls
(
    id         VARCHAR(10) PRIMARY KEY,
    source_url TEXT NOT NULL
);
CREATE TABLE users
(
    id       uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    login    VARCHAR(300) NOT NULL,
    password VARCHAR(300) NOT NULL,
    UNIQUE (login)
);

CREATE TABLE roles
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(300) NOT NULL,
    UNIQUE (name)
);

CREATE TABLE user_roles
(
    user_id uuid    NOT NULL,
    role_id INTEGER NOT NULL,
    UNIQUE (user_id, role_id),

    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT fk_role FOREIGN KEY (role_id) REFERENCES roles (id)
);

CREATE TABLE user_short_urls
(
    user_id      uuid        NOT NULL,
    short_url_id VARCHAR(10) NOT NULL,
    UNIQUE (user_id, short_url_id),

    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT fk_short_url FOREIGN KEY (short_url_id) REFERENCES short_urls (id) ON DELETE CASCADE
);

INSERT INTO users (login, password)
VALUES ('admin', '$2a$10$iGQaSNBgnFCxO4Rb7lV3FOstemWChxcZgQoeMxwTALZWnWvPy9j02'),
       ('superuser', '$2a$10$rCK8zDEAaFgULZ149p6YQuCdP1nqDUMu4nPJ.s/nB5roMZ0tkJIYG'),
       ('user', '$2a$10$IlwkZImj9jbypaG421SEh.JCyZzXD8FJQSWHZflq72QqRv6WCtK6u');

INSERT INTO roles (name)
VALUES ('Admin'),
       ('SuperUser'),
       ('User');

INSERT INTO user_roles (user_id, role_id)
VALUES ((select id from users where login = 'admin' limit 1), (select id from roles where name = 'Admin' limit 1)),
       ((select id from users where login = 'admin' limit 1), (select id from roles where name = 'SuperUser' limit 1)),
       ((select id from users where login = 'admin' limit 1), (select id from roles where name = 'User' limit 1)),
       ((select id from users where login = 'superuser' limit 1),
        (select id from roles where name = 'SuperUser' limit 1)),
       ((select id from users where login = 'user' limit 1), (select id from roles where name = 'User' limit 1));
package dev.yanovsky.myproject

import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import doobie.util.transactor.Transactor
import dev.yanovsky.myproject.components._
import dev.yanovsky.myproject.configs.AppConfig
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import dev.yanovsky.myproject.http.Api
import dev.yanovsky.myproject.daos.{ShortUrlDao, UserDao}
import tofu.logging.Logs

class Application(implicit config: AppConfig) extends StrictLogging {
  def run: IO[Unit] = {
    val serverR = for {
      implicit0(logs: Logs[IO, WithMyContext])  <- LogsComponent.init
      implicit0(metrics: Metrics)               <- MetricsComponent.init
      implicit0(xa: Transactor[WithMyContext])  <- DbComponent.init
      daos                                      <- DaoComponent.init
      implicit0(shortUrlDao: ShortUrlDao)        = daos.shortUrlDao
      implicit0(userDao: UserDao[WithMyContext]) = daos.userDao
      implicit0(api: Api)                       <- ApiComponent.init
      server                                    <- ServerComponent.init
    } yield server

    serverR.use { server =>
      for {
        _ <- IO(logger.info(s"Server started on http://${server.address.getHostName}:${server.address.getPort}"))
        _ <- IO(logger.info(s"Docs url: http://${server.address.getHostName}:${server.address.getPort}/docs"))
        _ <- IO.never[Unit]
      } yield ()
    }
  }
}

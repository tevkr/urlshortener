package dev.yanovsky.myproject

import cats.effect.{IO, IOApp}
import dev.yanovsky.myproject.configs._
import pureconfig._
import pureconfig.generic.auto._
import pureconfig.module.catseffect.syntax._

object Main extends IOApp.Simple {
  override def run: IO[Unit] =
    ConfigSource.default.loadF[IO, AppConfig].flatMap(implicit cfg => new Application().run)
}

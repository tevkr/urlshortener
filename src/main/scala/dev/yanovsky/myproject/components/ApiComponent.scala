package dev.yanovsky.myproject.components

import cats.effect.kernel.Resource.ExitCase
import cats.effect.{IO, Resource}
import com.typesafe.scalalogging.StrictLogging
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import dev.yanovsky.myproject.core.SimpleRolePolicy
import dev.yanovsky.myproject.http.Api
import dev.yanovsky.myproject.daos.{ShortUrlDao, UserDao}

object ApiComponent extends StrictLogging {
  def init(implicit
    shortUrlDao: ShortUrlDao,
    userDao: UserDao[WithMyContext],
    metrics: Metrics
  ): Resource[IO, Api] =
    Resource.makeCase(
      IO(new Api(shortUrlDao, userDao, new SimpleRolePolicy[WithMyContext], metrics))
    ) { (api, reason) =>
      api.stop >> (reason match {
        case ExitCase.Succeeded  => IO(logger.info("Complete"))
        case ExitCase.Errored(e) => IO(logger.error(e.getMessage, e))
        case ExitCase.Canceled   => IO(logger.warn("Stopped"))
      })
    }
}

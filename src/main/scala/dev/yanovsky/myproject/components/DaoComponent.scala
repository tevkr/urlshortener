package dev.yanovsky.myproject.components

import cats.effect.{IO, Resource}
import dev.yanovsky.myproject.configs.DaoConfig
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import doobie.util.transactor.Transactor
import dev.yanovsky.myproject.daos._

object DaoComponent {
  case class Daos(shortUrlDao: ShortUrlDao, userDao: UserDao[WithMyContext])
  def init(implicit xa: Transactor[WithMyContext], config: DaoConfig): Resource[IO, Daos] =
    Resource.eval(
      IO.delay(
        Daos(new ShortUrlDaoLoggingDecorator(new ShortUrlDaoImpl(xa, config)), new UserDaoImpl[WithMyContext](xa))
      )
    )
}

package dev.yanovsky.myproject.components

import cats.effect.{IO, Resource}
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.metrics.prometheus.PrometheusMetricsTrackerFactory
import doobie.Transactor
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import tofu.lift.Lift
import dev.yanovsky.myproject.configs._
import dev.yanovsky.myproject.core.AppCtx.WithMyContext

object DbComponent {
  def init(implicit config: DbConfig, metrics: Metrics): Resource[IO, Transactor[WithMyContext]] =
    for {
      connEc <- ExecutionContexts.fixedThreadPool[IO](3)
      config <- Resource.eval(
                  IO.delay(new HikariConfig()).flatTap { cfg =>
                    IO.delay {
                      cfg.setPassword(config.password)
                      cfg.setUsername(config.user)
                      cfg.setJdbcUrl(config.url)
                      cfg.setMetricsTrackerFactory(new PrometheusMetricsTrackerFactory(metrics.collectorRegistry))
                    }
                  }
                )
      transactor <- HikariTransactor.fromHikariConfig[IO](config, connEc)
    } yield transactor.mapK(Lift[IO, WithMyContext].liftF)
}

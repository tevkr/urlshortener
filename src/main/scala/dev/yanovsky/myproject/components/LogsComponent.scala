package dev.yanovsky.myproject.components

import cats.effect.{IO, Resource}
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import tofu.logging.Logs

object LogsComponent {
  def init(): Resource[IO, Logs[IO, WithMyContext]] = Resource.pure(Logs.withContext[IO, WithMyContext])
}

package dev.yanovsky.myproject.components

import cats.effect.{IO, Resource}
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import io.prometheus.client.CollectorRegistry
import sttp.tapir.server.metrics.prometheus.PrometheusMetrics

case class Metrics(collectorRegistry: CollectorRegistry, tapirMetrics: PrometheusMetrics[WithMyContext])
object MetricsComponent {
  def init(): Resource[IO, Metrics] = Resource.eval(
    for {
      collectorRegistry <- IO(CollectorRegistry.defaultRegistry)
      tapirMetrics      <- IO(PrometheusMetrics.default[WithMyContext]("http", collectorRegistry))
    } yield Metrics(collectorRegistry, tapirMetrics)
  )
}

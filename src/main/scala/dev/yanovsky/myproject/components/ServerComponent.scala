package dev.yanovsky.myproject.components

import cats.effect.{IO, Resource}
import cats.implicits.toSemigroupKOps
import com.comcast.ip4s.Host
import dev.yanovsky.myproject.configs.AppConfig
import dev.yanovsky.myproject.http.Api
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Server
import sttp.tapir.server.http4s.Http4sServerInterpreter
import cats.effect.std.UUIDGen
import dev.yanovsky.myproject.core.AppCtx
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import org.http4s.Http
import org.http4s.implicits._
import sttp.tapir.server.http4s.{Http4sDefaultServerLog, Http4sServerOptions}
import tofu.WithRun
import tofu.lift.Lift
import tofu.logging.Logs

case class ShortUrlsHttpServer(routes: Http[IO, IO])

object ServerComponent {
  def init(implicit
    config: AppConfig,
    metrics: Metrics,
    api: Api,
    logs: Logs[IO, WithMyContext]
  ): Resource[IO, Server] =
    Resource.eval {
      Logs.provide[IO, WithMyContext] { implicit logging =>
        val serverLog = Http4sDefaultServerLog[WithMyContext]
          .copy(logWhenReceived = true)
          .doLogWhenReceived(s => logging.info(s"Received request $s"))
          .doLogWhenHandled((s, exOpt) =>
            if (exOpt.isEmpty) logging.info(s"Request handled successfully $s")
            else logging.error(s"Failed to handle request $s", exOpt.get)
          )
          .doLogExceptions((s, err) => logging.error(s"Error occurred during request handling $s", err))
        val options =
          Http4sServerOptions
            .customiseInterceptors[WithMyContext]
            .serverLog(serverLog)
            .metricsInterceptor(metrics.tapirMetrics.metricsInterceptor())

        ShortUrlsHttpServer(
          api.endpoints
            .map(ep =>
              Http4sServerInterpreter[WithMyContext](options.options)
                .toRoutes(ep)
                .translate[IO](tofu.syntax.funk.funK(runWithContext _))(Lift[IO, WithMyContext].liftF)
            )
            .reduce(_ <+> _)
            .orNotFound
        )
      }
    }
      .flatMap(todoServer =>
        EmberServerBuilder
          .default[IO]
          .withHost(Host.fromString("0.0.0.0").get)
          .withPort(config.port)
          .withHttpApp(todoServer.routes)
          .build
      )
  def runWithContext[A](comp: WithMyContext[A]): IO[A] =
    for {
      ctx    <- UUIDGen.randomString[IO].map(AppCtx(_))
      result <- WithRun[WithMyContext, IO, AppCtx].runContextK(ctx)(comp)
    } yield result
}

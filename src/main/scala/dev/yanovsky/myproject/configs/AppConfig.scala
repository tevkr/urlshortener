package dev.yanovsky.myproject.configs

import com.comcast.ip4s.Port
import pureconfig.ConfigReader
import pureconfig.generic.semiauto.deriveReader

case class AppConfig(db: DbConfig, dao: DaoConfig, port: Port)

object AppConfig {
  implicit val portReader: ConfigReader[Port]        = ConfigReader.fromStringOpt(Port.fromString)
  implicit val configReader: ConfigReader[AppConfig] = deriveReader[AppConfig]
}

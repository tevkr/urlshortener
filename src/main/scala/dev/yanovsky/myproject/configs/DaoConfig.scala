package dev.yanovsky.myproject.configs

import pureconfig.ConfigReader
import pureconfig.generic.semiauto.deriveReader

case class DaoConfig(idLength: Int)

object DaoConfig {
  implicit val daoConfigReader: ConfigReader[DaoConfig]            = deriveReader[DaoConfig]
  implicit def daoConfigExt(implicit config: AppConfig): DaoConfig = config.dao
}

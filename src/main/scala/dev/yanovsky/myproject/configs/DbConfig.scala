package dev.yanovsky.myproject.configs

import pureconfig.ConfigReader
import pureconfig.generic.semiauto.deriveReader

case class DbConfig(url: String, user: String, password: String)

object DbConfig {
  implicit val dbConfigReader: ConfigReader[DbConfig]            = deriveReader[DbConfig]
  implicit def dbConfigExt(implicit config: AppConfig): DbConfig = config.db
}

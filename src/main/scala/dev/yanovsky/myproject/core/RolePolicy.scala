package dev.yanovsky.myproject.core

import dev.yanovsky.myproject.models.User

trait RolePolicy[F[_]] {
  def haveAdminRights(user: User): F[Boolean]
  def canCreateShortUrl(user: User): F[Boolean]
  def canModifyShortUrl(user: User, shortUrlId: String): F[Boolean]
}

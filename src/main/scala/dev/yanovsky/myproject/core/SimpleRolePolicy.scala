package dev.yanovsky.myproject.core

import cats.Applicative
import dev.yanovsky.myproject.models.{Role, ShortUrl, User}

class SimpleRolePolicy[F[_]: Applicative] extends RolePolicy[F] {
  import cats.syntax.applicative._
  override def haveAdminRights(user: User): F[Boolean] = user.roles.contains(Role.Admin).pure

  override def canCreateShortUrl(user: User): F[Boolean] =
    (user.roles.contains(Role.SuperUser) || user.roles.contains(Role.User) && user.shortUrls.length < 10).pure
  override def canModifyShortUrl(user: User, shortUrlId: String): F[Boolean] =
    user.shortUrls.exists(_.id == shortUrlId).pure
}

package dev.yanovsky.myproject.daos

import com.typesafe.scalalogging.StrictLogging
import dev.yanovsky.myproject.models.ShortUrl
import dev.yanovsky.myproject.configs.DaoConfig
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import doobie.util.transactor.Transactor
import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext

import java.util.UUID

trait ShortUrlDao {
  def getAll(): WithMyContext[Set[ShortUrl]]
  def getById(id: String): WithMyContext[Either[String, ShortUrl]]
  def create(userId: UUID, shortUrl: ShortUrl): WithMyContext[Either[String, ShortUrl]]
  def update(shortUrl: ShortUrl): WithMyContext[Either[String, Unit]]
  def remove(id: String): WithMyContext[Either[String, Unit]]
}

class ShortUrlDaoLoggingDecorator(shortUrlDaoImpl: ShortUrlDaoImpl) extends ShortUrlDao with StrictLogging {
  override def getAll(): WithMyContext[Set[ShortUrl]] = {
    logger.info("Getting all short urls")
    shortUrlDaoImpl.getAll()
  }

  override def getById(id: String): WithMyContext[Either[String, ShortUrl]] = {
    logger.info(s"Getting short url by id = $id")
    shortUrlDaoImpl
      .getById(id)
      .map {
        case result @ Left(message) =>
          logger.info(message)
          result
        case result => result
      }
  }

  override def create(userId: UUID, shortUrl: ShortUrl): WithMyContext[Either[String, ShortUrl]] = {
    logger.info("Creating short url")
    shortUrlDaoImpl.create(userId, shortUrl)
  }

  override def update(shortUrl: ShortUrl): WithMyContext[Either[String, Unit]] = {
    logger.info("Updating short url")
    shortUrlDaoImpl
      .update(shortUrl)
      .map {
        case result @ Left(message) =>
          logger.info(message)
          result
        case result => result
      }
  }

  override def remove(id: String): WithMyContext[Either[String, Unit]] = {
    logger.info(s"Removing short url by id = $id")
    shortUrlDaoImpl
      .remove(id)
      .map {
        case result @ Left(message) =>
          logger.info(message)
          result
        case result => result
      }
  }
}

final class ShortUrlDaoImpl(xa: Transactor[WithMyContext], config: DaoConfig) extends ShortUrlDao {

  import ShortUrlDaoImpl._
  import doobie.implicits._

  val dc = new DoobieContext.Postgres(SnakeCase)
  import dc.{SqlInfixInterpolator => _, _}

  private val shortUrls = quote {
    querySchema[ShortUrl]("short_urls", _.id -> "id", _.sourceUrl -> "source_url")
  }
  case class UserShortUrl(userId: UUID, shortUrlId: String)
  val userShortUrls = quote {
    querySchema[UserShortUrl]("user_short_urls", _.userId -> "user_id", _.shortUrlId -> "short_url_id")
  }
  override def getAll(): WithMyContext[Set[ShortUrl]] =
    run(shortUrls).transact(xa).map(_.toSet)

  override def getById(id: String): WithMyContext[Either[String, ShortUrl]] =
    run(shortUrls.filter(_.id == lift(id)).take(1)).transact(xa).map {
      case List(shortUrl) => Right(shortUrl)
      case _              => Left(s"Short url with id = $id not found.")
    }

  override def create(userId: UUID, shortUrl: ShortUrl): WithMyContext[Either[String, ShortUrl]] =
    genRandStr[WithMyContext](config.idLength).flatMap { str =>
      val createdShortUrl = ShortUrl(str, shortUrl.sourceUrl)
      val transaction = for {
        x <- run(shortUrls.insertValue(lift(createdShortUrl)))
        _ <- run(userShortUrls.insertValue(lift(UserShortUrl(userId, createdShortUrl.id))))
      } yield ()

      transaction.transact(xa).map(_ => Right(createdShortUrl))
    }

  override def update(shortUrl: ShortUrl): WithMyContext[Either[String, Unit]] =
    run(
      shortUrls
        .filter(_.id == lift(shortUrl.id))
        .update(r => r.sourceUrl -> lift(shortUrl.sourceUrl))
    ).transact(xa).map {
      case 0 => Left(s"Short url with id = ${shortUrl.id} not found.")
      case _ => Right(())
    }

  override def remove(id: String): WithMyContext[Either[String, Unit]] =
    run(shortUrls.filter(_.id == lift(id)).delete).transact(xa).map {
      case 0 => Left(s"Short url with id = $id not found.")
      case _ => Right(())
    }
}

object ShortUrlDaoImpl {
  import cats.effect.Sync
  import scala.util.Random
  def genRandStr[F[_]: Sync](length: Int): F[String] =
    Sync[F].delay(Random.alphanumeric.take(length).mkString)
}

package dev.yanovsky.myproject.daos

import cats.effect.kernel.MonadCancelThrow
import cats.implicits.{toFlatMapOps, toTraverseOps}
import doobie.Transactor
import doobie.implicits._
import io.getquill.SnakeCase
import dev.yanovsky.myproject.models.{Role, ShortUrl, User}
import org.mindrot.jbcrypt.BCrypt

import java.util.UUID

trait UserDao[F[_]] {
  def get(login: String): F[Option[User]]
  def get(id: UUID): F[Option[User]]
  def create(user: User): F[UUID]
  def put(userId: UUID, user: User): F[Unit]
  def delete(userId: UUID): F[Unit]
}

class UserDaoImpl[F[_]: MonadCancelThrow](xa: Transactor[F]) extends UserDao[F] {
  import UserDaoImpl._
  import cats.syntax.functor._
  override def get(login: String): F[Option[User]] =
    (for {
      user      <- findQuery(login)
      roles     <- rolesQuery(login)
      shortUrls <- shortUrlsQuery(login)
    } yield user.map(u => User(u.id, u.login, u.password, roles.map(_.name).flatMap(Role.withNameOption), shortUrls)))
      .transact(xa)

  override def get(id: UUID): F[Option[User]] =
    (for {
      user      <- findQuery(id)
      roles     <- rolesQuery(id)
      shortUrls <- shortUrlsQuery(id)
    } yield user.map(u => User(u.id, u.login, u.password, roles.map(_.name).flatMap(Role.withNameOption), shortUrls)))
      .transact(xa)
  override def create(user: User): F[UUID] =
    (for {
      uuid <- createQuery(user)
      _    <- setRolesToUserQuery(user).traverse(el => el)
    } yield uuid).transact(xa)

  override def put(userId: UUID, user: User): F[Unit] =
    (for {
      _ <- putQuery(userId, user)
      _ <- setRolesToUserQuery(user).traverse(el => el)
    } yield ()).transact(xa).void

  override def delete(userId: UUID): F[Unit] = deleteQuery(userId).transact(xa).void
}

object UserDaoImpl {
  case class UserRole(userId: UUID, roleId: Int)
  case class UserRecord(id: UUID, login: String, password: String)
  case class RoleRecord(id: Int, name: String)
  case class UserShortUrl(userId: UUID, shortUrlId: String)

  import io.getquill.Literal
  import io.getquill.doobie.DoobieContext

  val dc = new DoobieContext.Postgres(SnakeCase)

  import dc._

  val users = quote {
    querySchema[UserRecord]("users")
  }

  val roles = quote {
    querySchema[RoleRecord]("roles")
  }

  val userRoles = quote {
    querySchema[UserRole]("user_roles")
  }

  val shortUrls = quote {
    querySchema[ShortUrl]("short_urls", _.id -> "id", _.sourceUrl -> "source_url")
  }

  val userShortUrls = quote {
    querySchema[UserShortUrl]("user_short_urls", _.userId -> "user_id", _.shortUrlId -> "short_url_id")
  }

  def findQuery(login: String) =
    run(quote(users.filter(_.login == lift(login)).take(1))).map(_.headOption)

  def findQuery(id: UUID) =
    run(quote(users.filter(_.id == lift(id)).take(1))).map(_.headOption)

  def findQueryWithRoles(login: String) = {
    val q = quote {
      for {
        user     <- users.filter(_.login == lift(login)).take(1)
        role     <- roles
        userRole <- userRoles if role.id == userRole.roleId && user.id == userRole.userId
      } yield (user, role)
    }

    run(q)
  }
  def createQuery(user: User) = {
    val salt           = BCrypt.gensalt()
    val hashedPassword = BCrypt.hashpw(user.password, salt)
    run(users.insert(_.login -> lift(user.login), _.password -> lift(hashedPassword)).returningGenerated(_.id))
  }

  def putQuery(userId: UUID, user: User) = {
    val salt           = BCrypt.gensalt()
    val hashedPassword = BCrypt.hashpw(user.password, salt)
    run(users.filter(_.id == lift(userId)).update(_.login -> lift(user.login), _.password -> lift(hashedPassword)))
  }

  def deleteQuery(userId: UUID) =
    run(quote(users.filter(_.id == lift(userId)).delete))

  def rolesQuery(login: String) = {
    val q = quote {
      for {
        user     <- users.filter(_.login == lift(login))
        role     <- roles
        userRole <- userRoles if role.id == userRole.roleId && user.id == userRole.userId
      } yield role
    }

    run(q)
  }

  def rolesQuery(id: UUID) = {
    val q = quote {
      for {
        user     <- users.filter(_.id == lift(id))
        role     <- roles
        userRole <- userRoles if role.id == userRole.roleId && user.id == userRole.userId
      } yield role
    }

    run(q)
  }

  def shortUrlsQuery(login: String) = {
    val q = quote {
      for {
        user         <- users.filter(_.login == lift(login))
        shortUrl     <- shortUrls
        userShortUrl <- userShortUrls if shortUrl.id == userShortUrl.shortUrlId && user.id == userShortUrl.userId
      } yield shortUrl
    }

    run(q)
  }

  def shortUrlsQuery(id: UUID) = {
    val q = quote {
      for {
        user         <- users.filter(_.id == lift(id))
        shortUrl     <- shortUrls
        userShortUrl <- userShortUrls if shortUrl.id == userShortUrl.shortUrlId && user.id == userShortUrl.userId
      } yield shortUrl
    }

    run(q)
  }

  def setRolesToUserQuery(user: User) = {
    val existingRoles = Role.values
    val rolesToAdd    = user.roles
    val rolesToRemove = existingRoles.filterNot(user.roles.contains)
    val addQueries    = rolesToAdd.map(addRoleQuery(user, _))
    val removeQueries = rolesToRemove.map(removeRoleQuery(user, _))
    (addQueries ++ removeQueries)
  }

  def addRoleQuery(user: User, role: Role) =
    for {
      userId <- run(quote(users.filter(_.login == lift(user.login)).map(_.id).take(1))).map(_.head)
      roleId <- run(quote(roles.filter(_.name == lift(role.entryName)).map(_.id).take(1))).map(_.head)
      _      <- run(quote(userRoles.insert(_.roleId -> lift(roleId), _.userId -> lift(userId)).onConflictIgnore))
    } yield ()

  def removeRoleQuery(user: User, role: Role) =
    for {
      userId <- run(quote(users.filter(_.login == lift(user.login)).map(_.id).take(1))).map(_.head)
      roleId <- run(quote(roles.filter(_.name == lift(role.entryName)).map(_.id).take(1))).map(_.head)
      _      <- run(quote(userRoles.filter(r => r.roleId == lift(roleId) && r.userId == lift(userId))).delete)
    } yield ()
}

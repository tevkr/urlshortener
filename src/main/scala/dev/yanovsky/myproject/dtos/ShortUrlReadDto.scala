package dev.yanovsky.myproject.dtos

import io.circe.{Decoder, Encoder}

case class ShortUrlReadDto(id: String, sourceUrl: String)

object ShortUrlReadDto {
  implicit val shortUrlEncoder: Encoder[ShortUrlReadDto] =
    Encoder.forProduct2("id", "sourceUrl")(shortUrl => (shortUrl.id, shortUrl.sourceUrl))
  implicit val shortUrlDecoder: Decoder[ShortUrlReadDto] = Decoder.forProduct2("id", "sourceUrl") {
    (id: String, sourceUrl: String) =>
      ShortUrlReadDto(id, sourceUrl)
  }
}

package dev.yanovsky.myproject.dtos

case class ShortUrlUpdateDto(sourceUrl: String)

package dev.yanovsky.myproject.dtos

case class ShortUrlWriteDto(sourceUrl: String)

package dev.yanovsky.myproject.dtos

import io.circe.{Decoder, Encoder}

import java.util.UUID

case class UserReadDto(id: UUID, login: String, password: String, roles: List[String])

object UserReadDto {
  implicit val userEncoder: Encoder[UserReadDto] =
    Encoder.forProduct4("id", "login", "password", "roles")(user => (user.id, user.login, user.password, user.roles))
  implicit val userDecoder: Decoder[UserReadDto] = Decoder.forProduct4("id", "login", "password", "roles") {
    (id: UUID, login: String, password: String, roles: List[String]) =>
      UserReadDto(id, login, password, roles)
  }
}

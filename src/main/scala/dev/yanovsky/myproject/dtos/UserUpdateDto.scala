package dev.yanovsky.myproject.dtos

import io.circe.{Decoder, Encoder}

case class UserUpdateDto(login: String, password: String, roles: List[String])

object UserUpdateDto {
  implicit val userEncoder: Encoder[UserUpdateDto] =
    Encoder.forProduct3("login", "password", "roles")(user => (user.login, user.password, user.roles))
  implicit val userDecoder: Decoder[UserUpdateDto] = Decoder.forProduct3("login", "password", "roles") {
    (login: String, password: String, roles: List[String]) =>
      UserUpdateDto(login, password, roles)
  }
}

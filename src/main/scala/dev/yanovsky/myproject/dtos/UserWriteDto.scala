package dev.yanovsky.myproject.dtos

import io.circe.{Decoder, Encoder}

case class UserWriteDto(login: String, password: String, roles: List[String])

object UserWriteDto {
  implicit val userEncoder: Encoder[UserWriteDto] =
    Encoder.forProduct3("login", "password", "roles")(user => (user.login, user.password, user.roles))
  implicit val userDecoder: Decoder[UserWriteDto] = Decoder.forProduct3("login", "password", "roles") {
    (login: String, password: String, roles: List[String]) =>
      UserWriteDto(login, password, roles)
  }
}

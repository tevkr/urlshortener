package dev.yanovsky.myproject.http

import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import dev.yanovsky.myproject.components.Metrics
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import dev.yanovsky.myproject.core.RolePolicy
import dev.yanovsky.myproject.daos.{ShortUrlDao, UserDao}
import dev.yanovsky.myproject.dtos.{ShortUrlReadDto, UserReadDto}
import dev.yanovsky.myproject.models.{Role, ShortUrl, User}
import dev.yanovsky.myproject.models.ShortUrl.{shortUrlToReadDto, writeDtoToShortUrl}
import dev.yanovsky.myproject.models.User.userToReadDto
import org.mindrot.jbcrypt.BCrypt
import sttp.tapir.model.UsernamePassword
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.swagger.bundle.SwaggerInterpreter

class Api(
  shortUrlDao: ShortUrlDao,
  userDao: UserDao[WithMyContext],
  rolePolicy: RolePolicy[WithMyContext],
  metrics: Metrics
) extends StrictLogging {

  import cats.syntax.applicative._
  import cats.syntax.either._
  private def authLogic(cred: UsernamePassword) =
    userDao.get(cred.username).map {
      case Some(user) if BCrypt.checkpw(cred.password.getOrElse(""), user.password) => Right(user)
      case _                                                                        => Left("User not found or password wrong")
    }

  // Hello world

  val getHelloWorld = Endpoints.helloEndpoint.serverLogicSuccess[WithMyContext] { _ =>
    s"Hello World!".pure[WithMyContext]
  }

  // Root

  val redirect = Endpoints.redirect
    .serverLogic[WithMyContext] { id =>
      shortUrlDao.getById(id).map {
        case Right(shortUrl) => Right(shortUrl.sourceUrl)
        case Left(error)     => Left(error)
      }
    }

  // User

  val getUser = Endpoints.getUser
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => id =>
      rolePolicy.haveAdminRights(user).flatMap {
        case true  => userDao.get(id).map(_.map(userToReadDto).toRight("User not found"))
        case false => s"Access denied".asLeft[UserReadDto].pure[WithMyContext]
      }
    }
  val createUser = Endpoints.createUser
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => writeDto =>
      rolePolicy.haveAdminRights(user).flatMap {
        case true =>
          userDao
            .create(writeDto)
            .map(uuid => Right(UserReadDto(uuid, writeDto.login, writeDto.password, writeDto.roles)))
        case false => s"Access denied".asLeft[UserReadDto].pure[WithMyContext]
      }
    }
  val updateUser = Endpoints.updateUser
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => idAndUpdateDto =>
      rolePolicy.haveAdminRights(user).flatMap {
        case true  => userDao.put(idAndUpdateDto._1, idAndUpdateDto._2).map(_.asRight[String])
        case false => s"Access denied".asLeft[Unit].pure[WithMyContext]
      }
    }
  val removeUser = Endpoints.removeUser
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => id =>
      rolePolicy.haveAdminRights(user).flatMap {
        case true  => userDao.delete(id).map(_.asRight[String])
        case false => s"Access denied".asLeft[Unit].pure[WithMyContext]
      }
    }

  // Short Url

  val getAllShortUrls = Endpoints.getAllShortUrls
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogicSuccess { _ => _ =>
      shortUrlDao.getAll().map(_.map(shortUrlToReadDto))
    }

  val getShortUrl = Endpoints.getShortUrl
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { _ => id =>
      shortUrlDao.getById(id).map(_.map(shortUrlToReadDto))
    }

  val createShortUrl = Endpoints.createShortUrl
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => shortUrlWriteDto =>
      rolePolicy.canCreateShortUrl(user).flatMap {
        case true =>
          if (!isUrl(shortUrlWriteDto.sourceUrl)) {
            s"${shortUrlWriteDto.sourceUrl} is not a URL.".asLeft[ShortUrlReadDto].pure[WithMyContext]
          } else {
            shortUrlDao.create(user.id, shortUrlWriteDto).map(_.map(shortUrlToReadDto))
          }
        case false => s"Access denied".asLeft[ShortUrlReadDto].pure[WithMyContext]
      }
    }

  val updateShortUrl = Endpoints.updateShortUrl
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => idAndUpdateDto =>
      rolePolicy.canModifyShortUrl(user, idAndUpdateDto._1).flatMap {
        case true =>
          if (!isUrl(idAndUpdateDto._2.sourceUrl)) {
            s"${idAndUpdateDto._2.sourceUrl} is not a URL.".asLeft[Unit].pure[WithMyContext]
          } else {
            shortUrlDao.update(ShortUrl(idAndUpdateDto._1, idAndUpdateDto._2.sourceUrl))
          }
        case false => s"Access denied".asLeft[Unit].pure[WithMyContext]
      }
    }

  val removeShortUrl = Endpoints.removeShortUrl
    .serverSecurityLogic[User, WithMyContext](authLogic)
    .serverLogic { user => id =>
      rolePolicy.canModifyShortUrl(user, id).flatMap {
        case true  => shortUrlDao.remove(id)
        case false => s"Access denied".asLeft[Unit].pure[WithMyContext]
      }
    }

  private def isUrl(input: String): Boolean = {
    val urlRegex = """^(https?|ftp)://[^\s/$.?#].[^\s]*$""".r
    urlRegex.findFirstMatchIn(input).isDefined
  }

  val serviceEndpoints =
    getHelloWorld :: redirect :: getAllShortUrls :: getShortUrl ::
      createShortUrl :: updateShortUrl :: removeShortUrl ::
      getUser :: createUser :: updateUser :: removeUser :: Nil
  val docEndpoints = SwaggerInterpreter().fromServerEndpoints[WithMyContext](serviceEndpoints, "URL Shortener", "1.0")

  val endpoints: List[ServerEndpoint[Any, WithMyContext]] =
    serviceEndpoints ++ docEndpoints :+ metrics.tapirMetrics.metricsEndpoint

  val stop: IO[Unit] = IO(logger.info("Stopping...")) >> IO.unit
}

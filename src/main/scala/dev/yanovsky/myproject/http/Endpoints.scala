package dev.yanovsky.myproject.http

import dev.yanovsky.myproject.http.endpoints.RootEndpoints
import dev.yanovsky.myproject.http.endpoints.api.{HelloEndpoint, ShortUrlsEndpoints, UsersEndpoints}
import sttp.tapir._

object Endpoints {
  /*
    GET /r/:id - перейти по сокращенной ссылке

    GET /api/v1/hello - получить Hello World!

    GET /api/v1/shorturls - получить все сокращенные ссылки
    GET /api/v1/shorturls/:id - получить сокращенну ссылку по идентификатору
    POST /api/v1/shorturls - создать сокращенную ссылку
    PUT /api/v1/shorturls/:id - обновить сокращенную ссылку по идентификатору
    DELETE /api/v1/shorturls/:id - удалить сокращенную ссылку по идентификатору

    GET /api/v1/users/:id - получить пользователя по идентификатору
    POST /api/v1/users - создать пользователя
    PUT /api/v1/users/:id - обновить пользователя по идентификатору
    DELETE /api/v1/users/:id - удалить пользователя по идентификатору
   */

  // API
  // Hello World
  val helloEndpoint = HelloEndpoint.helloEndpoint

  // User
  val createUser = UsersEndpoints.createUser
  val getUser    = UsersEndpoints.getUser
  val updateUser = UsersEndpoints.updateUser
  val removeUser = UsersEndpoints.removeUser

  // Short Url
  val getAllShortUrls = ShortUrlsEndpoints.getAllShortUrls
  val createShortUrl  = ShortUrlsEndpoints.createShortUrl
  val getShortUrl     = ShortUrlsEndpoints.getShortUrl
  val updateShortUrl  = ShortUrlsEndpoints.updateShortUrl
  val removeShortUrl  = ShortUrlsEndpoints.removeShortUrl

  // Root
  val redirect = RootEndpoints.redirect

  def get: List[AnyEndpoint] = List(
    helloEndpoint,
    redirect,
    getAllShortUrls,
    getShortUrl,
    createShortUrl,
    updateShortUrl,
    removeShortUrl,
    createUser,
    getUser,
    updateUser,
    removeUser
  )
}

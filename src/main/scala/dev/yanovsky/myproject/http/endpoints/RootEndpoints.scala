package dev.yanovsky.myproject.http.endpoints

import sttp.tapir._
import sttp.model.StatusCode

object RootEndpoints {
  /*
    GET /r/:id - перейти по сокращенной ссылке
   */
  private val shortUrlId = path[String]("id")
    .description("Идентификатор сокращенной ссылки")
    .example("9XnQ0saDMl")

  val redirect = endpoint
    .in("r" / shortUrlId)
    .get
    .out(statusCode(StatusCode.MovedPermanently))
    .out(header[String]("Location"))
    .errorOut(stringBody)
    .description("Перейти по сокращенной ссылке")

  def get: List[AnyEndpoint] = List(
    redirect
  )
}

package dev.yanovsky.myproject.http.endpoints.api

import sttp.tapir._

object HelloEndpoint {
  /*
    GET /api/v1/hello - получить Hello World!
   */
  val apiV1Endpoint = endpoint.in("api" / "v1").errorOut(stringBody)

  val helloEndpoint = apiV1Endpoint.in("hello").get.out(stringBody).tag("Hello World")

  def get: List[AnyEndpoint] = List(
    helloEndpoint
  )
}

package dev.yanovsky.myproject.http.endpoints.api

import dev.yanovsky.myproject.dtos.{ShortUrlReadDto, ShortUrlUpdateDto, ShortUrlWriteDto}
import sttp.tapir._
import sttp.tapir.json.circe._
import io.circe.generic.auto._
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.model.UsernamePassword

object ShortUrlsEndpoints {
  /*
    GET /api/v1/shorturls - получить все сокращенные ссылки
    GET /api/v1/shorturls/:id - получить сокращенну ссылку по идентификатору
    POST /api/v1/shorturls - создать сокращенную ссылку
    PUT /api/v1/shorturls/:id - обновить сокращенную ссылку по идентификатору
    DELETE /api/v1/shorturls/:id - удалить сокращенную ссылку по идентификатору
   */
  private val shortUrlId = path[String]("id")
    .description("Идентификатор сокращенной ссылки")
    .example("9XnQ0saDMl")
  private val shortUrlReadDto = jsonBody[ShortUrlReadDto]
    .description("Объект сокращенной ссылки")
    .example(ShortUrlReadDto("9XnQ0saDMl", "https://example.com/very/long/path/i/mean/very/long"))
  private val shortUrlsReadDto = jsonBody[Set[ShortUrlReadDto]]
    .description("Все сокращенные ссылки")
    .example(
      Set(
        ShortUrlReadDto("9XnQ0saDMl", "https://example.com/very/long/path/i/mean/very/long1"),
        ShortUrlReadDto("3js81hsdab", "https://example.com/very/long/path/i/mean/very/long2")
      )
    )
  private val shortUrlWriteDto = jsonBody[ShortUrlWriteDto]
    .description("Объект сокращенной ссылки")
    .example(ShortUrlWriteDto("https://example.com/very/long/path/i/mean/very/long"))
  private val shortUrlUpdateDto = jsonBody[ShortUrlUpdateDto]
    .description("Объект сокращенной ссылки")
    .example(ShortUrlUpdateDto("https://example.com/very/long/path/i/mean/very/long"))

  val apiV1Endpoint = endpoint
    .in("api" / "v1")
    .securityIn(auth.basic[UsernamePassword]())
    .errorOut(stringBody)
  val shortUrlsEndpoint = apiV1Endpoint.in("shorturls").tag("Short urls")
  val shortUrlEndpoint  = shortUrlsEndpoint.in(shortUrlId)

  val getAllShortUrls = shortUrlsEndpoint.get.out(shortUrlsReadDto).description("Получить все сокращенные ссылки")
  val createShortUrl =
    shortUrlsEndpoint.post.in(shortUrlWriteDto).out(shortUrlReadDto).description("Создать сокращенную ссылку")
  val getShortUrl =
    shortUrlEndpoint.get.out(shortUrlReadDto).description("Получить сокращенну ссылку по идентификатору")
  val updateShortUrl =
    shortUrlEndpoint.put.in(shortUrlUpdateDto).description("Обновить сокращенную ссылку по идентификатору")
  val removeShortUrl = shortUrlEndpoint.delete.description("Удалить сокращенную ссылку по идентификатору")

  def get: List[AnyEndpoint] = List(
    getAllShortUrls,
    getShortUrl,
    createShortUrl,
    updateShortUrl,
    removeShortUrl
  )
}

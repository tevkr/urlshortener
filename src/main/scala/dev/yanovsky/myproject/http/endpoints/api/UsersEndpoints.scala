package dev.yanovsky.myproject.http.endpoints.api

import dev.yanovsky.myproject.dtos.{UserReadDto, UserUpdateDto, UserWriteDto}
import sttp.tapir._
import sttp.tapir.json.circe._
import io.circe.generic.auto._
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.model.UsernamePassword

import java.util.UUID

object UsersEndpoints {
  /*
    GET /api/v1/users - получить всех пользователей
    GET /api/v1/users/:id - получить пользователя по идентификатору
    POST /api/v1/users - создать пользователя
    PUT /api/v1/users/:id - обновить пользователя по идентификатору
    DELETE /api/v1/users/:id - удалить пользователя по идентификатору
   */

  private val userId = path[UUID]("id")
    .description("Идентификатор пользователя")
    .example(UUID.randomUUID())

  private val userReadDto = jsonBody[UserReadDto]
    .description("Объект пользователя")
    .example(
      UserReadDto(UUID.randomUUID(), "login", "password", List("Admin", "SuperUser", "User"))
    )

  private val usersReadDto = jsonBody[Set[UserReadDto]]
    .description("Все пользователи")
    .example(
      Set(
        UserReadDto(UUID.randomUUID(), "login1", "password1", List("Admin", "SuperUser", "User")),
        UserReadDto(UUID.randomUUID(), "login2", "password2", List("User"))
      )
    )

  private val userWriteDto = jsonBody[UserWriteDto]
    .description("Объект пользователя")
    .example(UserWriteDto("login", "password", List("Admin", "SuperUser", "User")))

  private val userUpdateDto = jsonBody[UserUpdateDto]
    .description("Объект пользователя")
    .example(UserUpdateDto("login", "password", List("Admin", "SuperUser", "User")))

  val apiV1Endpoint = endpoint
    .in("api" / "v1")
    .securityIn(auth.basic[UsernamePassword]())
    .errorOut(stringBody)

  val usersEndpoint = apiV1Endpoint.in("users").tag("Users")
  val userEndpoint  = usersEndpoint.in(userId)

  val createUser = usersEndpoint.post.in(userWriteDto).out(userReadDto).description("Создать пользователя")
  val getUser    = userEndpoint.get.out(userReadDto).description("Получить пользователя по идентификатору")
  val updateUser = userEndpoint.put.in(userUpdateDto).description("Обновить пользователя по идентификатору")
  val removeUser = userEndpoint.delete.description("Удалить пользователя по идентификатору")

  def get: List[AnyEndpoint] = List(
    getUser,
    createUser,
    updateUser,
    removeUser
  )
}

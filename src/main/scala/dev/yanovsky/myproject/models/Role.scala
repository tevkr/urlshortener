package dev.yanovsky.myproject.models

import enumeratum._
import io.circe.{Decoder, Encoder}
import sttp.tapir.Schema

sealed trait Role extends EnumEntry

object Role extends Enum[Role] {
  val values = findValues
  case object Admin     extends Role // Может добавлять пользователей
  case object SuperUser extends Role // Может создавать бесконечное количество сокращенных ссылок
  case object User      extends Role // Может создавать до 10 сокращенных ссылок

  implicit val e: Encoder[Role] = Encoder.encodeString.contramap(_.entryName)
  implicit val d: Decoder[Role] = Decoder.decodeString.emap(str =>
    Role.withNameOption(str) match {
      case Some(value) => Right(value)
      case None        => Left(s"Role $str nof found")
    }
  )
  implicit val s: Schema[Role] = Schema.derived
}

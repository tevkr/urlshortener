package dev.yanovsky.myproject.models

import dev.yanovsky.myproject.dtos.{ShortUrlReadDto, ShortUrlUpdateDto, ShortUrlWriteDto}
import io.circe.{Decoder, Encoder}
import sttp.tapir.Schema
case class ShortUrl(id: String, sourceUrl: String)
object ShortUrl {
  implicit def shortUrlToReadDto(shortUrl: ShortUrl): ShortUrlReadDto = ShortUrlReadDto(shortUrl.id, shortUrl.sourceUrl)
  implicit def writeDtoToShortUrl(shortUrlWriteDto: ShortUrlWriteDto): ShortUrl =
    ShortUrl("", shortUrlWriteDto.sourceUrl)
  implicit def updateDtoToShortUrl(shortUrlUpdateDto: ShortUrlUpdateDto): ShortUrl =
    ShortUrl("", shortUrlUpdateDto.sourceUrl)

  implicit val e: Encoder[ShortUrl] = io.circe.generic.semiauto.deriveEncoder
  implicit val d: Decoder[ShortUrl] = io.circe.generic.semiauto.deriveDecoder
  implicit val s: Schema[ShortUrl]  = Schema.derived
}

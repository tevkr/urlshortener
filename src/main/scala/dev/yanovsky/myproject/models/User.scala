package dev.yanovsky.myproject.models

import dev.yanovsky.myproject.dtos.{UserReadDto, UserUpdateDto, UserWriteDto}
import io.circe.{Decoder, Encoder}
import sttp.tapir.Schema

import java.util.UUID

case class User(id: UUID, login: String, password: String, roles: List[Role], shortUrls: List[ShortUrl])

object User {
  implicit def userToReadDto(user: User): UserReadDto =
    UserReadDto(user.id, user.login, user.password, user.roles.map(_.entryName))
  implicit def writeDtoToUser(userWriteDto: UserWriteDto): User =
    User(
      UUID.randomUUID(),
      userWriteDto.login,
      userWriteDto.password,
      userWriteDto.roles.flatMap(Role.withNameOption),
      List[ShortUrl]()
    )
  implicit def updateDtoToUser(userUpdateDto: UserUpdateDto): User =
    User(
      UUID.randomUUID(),
      userUpdateDto.login,
      userUpdateDto.password,
      userUpdateDto.roles.flatMap(Role.withNameOption),
      List[ShortUrl]()
    )

  implicit val e: Encoder[ShortUrl] = io.circe.generic.semiauto.deriveEncoder
  implicit val d: Decoder[ShortUrl] = io.circe.generic.semiauto.deriveDecoder
  implicit val s: Schema[ShortUrl]  = Schema.derived
}

package dev.yanovsky.myproject

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import cats.implicits._
import com.dimafeng.testcontainers.PostgreSQLContainer
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import dev.yanovsky.myproject.components.Metrics
import dev.yanovsky.myproject.configs.AppConfig
import dev.yanovsky.myproject.core.{AppCtx, SimpleRolePolicy}
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import dev.yanovsky.myproject.daos.{ShortUrlDaoImpl, UserDaoImpl}
import dev.yanovsky.myproject.dtos.{UserReadDto, UserUpdateDto, UserWriteDto}
import dev.yanovsky.myproject.http.Api
import dev.yanovsky.myproject.models.User.userToReadDto
import dev.yanovsky.myproject.models.{ShortUrl, User}
import doobie.implicits._
import doobie.util.transactor.Transactor
import io.circe.parser.decode
import io.circe.syntax.EncoderOps
import io.prometheus.client.CollectorRegistry
import org.flywaydb.core.Flyway
import org.mindrot.jbcrypt.BCrypt
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.testcontainers.utility.DockerImageName
import pureconfig._
import pureconfig.module.catseffect.syntax._
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{UriContext, basicRequest}
import sttp.model.StatusCode
import sttp.tapir.integ.cats.CatsMonadError
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.metrics.prometheus.PrometheusMetrics
import sttp.tapir.server.stub.TapirStubInterpreter

import java.util.UUID

class ApiSpec extends AnyFlatSpec with Matchers with EitherValues with TestContainerForAll {
  override val containerDef = PostgreSQLContainer.Def(
    dockerImageName = DockerImageName.parse("postgres:15.1"),
    databaseName = "testcontainer-scala",
    username = "scala",
    password = "scala"
  )
  val collectorRegistry = CollectorRegistry.defaultRegistry
  val tapirMetrics      = PrometheusMetrics.default[WithMyContext]("ApiSpecMetrics", collectorRegistry)

  class Context(container: PostgreSQLContainer) {
    Class.forName(container.driverClassName)

    val flywayConfig = Flyway.configure
      .locations("migrations/")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()
    val transactor =
      Transactor
        .fromDriverManager[WithMyContext](
          container.driverClassName,
          container.jdbcUrl,
          container.username,
          container.password
        )
    val interpreter = TapirStubInterpreter(SttpBackendStub(new CatsMonadError[WithMyContext]()))
    val config      = ConfigSource.default.loadF[IO, AppConfig].unsafeRunSync()
    val shortUrlDao = new ShortUrlDaoImpl(transactor, config.dao)
    val userDao     = new UserDaoImpl[WithMyContext](transactor)
    val testUser: User = userDao
      .get("user")
      .map(_.map(u => u))
      .run(appCtx)
      .unsafeRunSync()
      .getOrElse(throw new RuntimeException("Migrations fail"))
    val adminId: UUID = userDao
      .get("admin")
      .map(_.map(_.id))
      .run(appCtx)
      .unsafeRunSync()
      .getOrElse(throw new RuntimeException("Migrations fail"))
    val policy = new SimpleRolePolicy[WithMyContext]

    val metrix = Metrics(collectorRegistry, tapirMetrics)
    val api    = new Api(shortUrlDao, userDao, policy, metrix)

    val shortUrls = Set[ShortUrl](
      ShortUrl("1111111111", "https://example.com/1"),
      ShortUrl("2222222222", "https://example.com/2")
    )
    def populate = {
      val insertShortUrlsQueries = shortUrls.map { url =>
        sql"INSERT INTO short_urls (id, source_url) VALUES (${url.id}, ${url.sourceUrl})".update.run
      }
      val insertUserShortUrlsQueries = shortUrls.map { url =>
        sql"INSERT INTO user_short_urls (user_id, short_url_id) VALUES (${adminId.toString}::uuid, ${url.id})".update.run
      }
      val combinedQuery = (insertShortUrlsQueries ++ insertUserShortUrlsQueries).toList.traverse(_.void)
      combinedQuery.transact(transactor)
    }

    val testCredentials = java.util.Base64.getEncoder.encodeToString("admin:admin".getBytes("UTF-8"))

    val appCtx   = AppCtx("ApiSpec")
    def truncate = sql"TRUNCATE TABLE short_urls CASCADE".update.run.transact(transactor).run(appCtx)
    def buildBackend[A, U, I, E, O, R](serverEndpoint: ServerEndpoint.Full[A, U, I, E, O, R, WithMyContext]) =
      interpreter.whenServerEndpoint(serverEndpoint).thenRunLogic().backend()

    private val baseUrl          = s"http://localhost:${config.port}"
    private val apiV1Path        = s"$baseUrl/api/v1"
    val helloWorldPath           = s"$apiV1Path/hello"
    val shortUrlsPath            = s"$apiV1Path/shorturls"
    val usersPath                = s"$apiV1Path/users"
    def shortUrlPath(id: String) = s"$shortUrlsPath/$id"
    def userPath(id: String)     = s"$usersPath/$id"
    def redirectPath(id: String) = s"/r/$id"
  }

  "GET /api/v1/hello" should "return 'Hello World!' message" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.getHelloWorld)

      val response =
        basicRequest.header("Authorization", s"Basic $testCredentials").get(uri"${helloWorldPath}").send(backendStub)
      response.map(_.body.value shouldBe "Hello World!").run(appCtx).unsafeRunSync()
    })
  }

  "GET /api/v1/shorturls" should "return all short urls" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getAllShortUrls)

      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .get(uri"$shortUrlsPath")
        .send(backendStub)
      val shortUrlsJson = shortUrls.asJson
        .toString()
        .filterNot(_.isWhitespace)
      response.map(_.body.value shouldBe shortUrlsJson).run(appCtx).unsafeRunSync()
    })
  }

  "GET /api/v1/shorturls/:id" should "return short url by object id" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getShortUrl)
      val testId      = "1111111111"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .get(uri"${shortUrlPath(testId)}")
        .send(backendStub)
      val shortUrlJson = shortUrls
        .find(_.id == testId)
        .get
        .asJson
        .toString()
        .filterNot(_.isWhitespace)
      response.map(_.body.value shouldBe shortUrlJson).run(appCtx).unsafeRunSync()
    })
  }

  it should "return error if id not found" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getShortUrl)
      val testId      = "testId"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .get(uri"${shortUrlPath(testId)}")
        .send(backendStub)
      response.map(_.body shouldBe Left(s"Short url with id = $testId not found.")).run(appCtx).unsafeRunSync()
    })
  }

  "POST /api/v1/shorturls" should "return created ShortUrl object" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.createShortUrl)
      val testSourceUrl = "https://google.com"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .post(uri"$shortUrlsPath")
        .body(payload)
        .send(backendStub)
      response
        .map(resp =>
          decode[ShortUrl](resp.body.value).map { shortUrl =>
            shortUrl.sourceUrl shouldBe testSourceUrl
            shortUrlDao.getById(shortUrl.id).run(appCtx).unsafeRunSync() match {
              case Right(shortUrlReadDto) =>
                shortUrlReadDto shouldBe shortUrl
              case Left(errorMessage) =>
                fail(errorMessage)
            }
          }
        )
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error if sourceUrl is not presented" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.createShortUrl)
      val payload     = s"{\"something\": \"test\"}"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .post(uri"$shortUrlsPath")
        .body(payload)
        .send(backendStub)
      response
        .map(_.body shouldBe Left(s"Invalid value for: body (Missing required field at 'sourceUrl')"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error if sourceUrl is not a Url" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.createShortUrl)
      val testSourceUrl = "test"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .post(uri"$shortUrlsPath")
        .body(payload)
        .send(backendStub)
      response.map(_.body shouldBe Left(s"$testSourceUrl is not a URL.")).run(appCtx).unsafeRunSync()
    })
  }

  "PUT /api/v1/shorturls/:id" should "change object" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.updateShortUrl)
      val testId        = "1111111111"
      val testSourceUrl = "http://test.com"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      (populate >>
        basicRequest
          .header("Authorization", s"Basic $testCredentials")
          .put(uri"${shortUrlPath(testId)}")
          .body(payload)
          .send(backendStub) >>
        shortUrlDao.getById(testId)).run(appCtx).unsafeRunSync() match {
        case Right(shortUrlReadDto) =>
          shortUrlReadDto.sourceUrl shouldBe testSourceUrl
        case Left(errorMessage) =>
          fail(errorMessage)
      }
    })
  }

  it should "return error if id not found" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.updateShortUrl)
      val testId        = "testId"
      val testSourceUrl = "http://test.com"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .put(uri"${shortUrlPath(testId)}")
        .body(payload)
        .send(backendStub)
      response.map(_.body shouldBe Left("Access denied")).run(appCtx).unsafeRunSync()
    })
  }

  it should "return error if sourceUrl is not presented" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.updateShortUrl)
      val testId        = "1111111111"
      val testSourceUrl = "http://test.com"
      val payload       = s"{\"something\": \"$testSourceUrl\"}"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .put(uri"${shortUrlPath(testId)}")
        .body(payload)
        .send(backendStub)
      response
        .map(_.body shouldBe Left(s"Invalid value for: body (Missing required field at 'sourceUrl')"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error if sourceUrl is not a Url" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.updateShortUrl)
      val testId        = "1111111111"
      val testSourceUrl = "test"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .put(uri"${shortUrlPath(testId)}")
        .body(payload)
        .send(backendStub)
      response.map(_.body shouldBe Left(s"$testSourceUrl is not a URL.")).run(appCtx).unsafeRunSync()
    })
  }

  "DELETE /api/v1/shorturls/:id" should "delete object" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.removeShortUrl)
      val testId      = "1111111111"
      (populate >>
        basicRequest
          .header("Authorization", s"Basic $testCredentials")
          .delete(uri"${shortUrlPath(testId)}")
          .send(backendStub) >>
        shortUrlDao.getById(testId)).run(appCtx).unsafeRunSync() match {
        case Right(shortUrlReadDto) =>
          fail(s"$shortUrlReadDto should be deleted.")
        case Left(errorMessage) =>
          errorMessage shouldBe s"Short url with id = $testId not found."
      }
    })
  }

  it should "return error if id not found" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.removeShortUrl)
      val testId      = "test"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .delete(uri"${shortUrlPath(testId)}")
        .send(backendStub)
      response.map(_.body shouldBe Left("Access denied")).run(appCtx).unsafeRunSync()
    })
  }

  "GET /r/:id" should "return redirect page" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.redirect)
      val testId      = "1111111111"
      val response    = populate >> basicRequest.get(uri"${redirectPath(testId)}").send(backendStub)
      response.map { resp =>
        resp.code shouldBe StatusCode.MovedPermanently
        resp.headers("Location").head shouldBe "https://example.com/1"
      }.run(appCtx).unsafeRunSync()
    })
  }

  it should "return error if id not found" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.redirect)
      val testId      = "test"
      val response    = populate >> basicRequest.get(uri"${redirectPath(testId)}").send(backendStub)
      response.map(_.body shouldBe Left(s"Short url with id = $testId not found.")).run(appCtx).unsafeRunSync()
    })
  }

  "If service has crashed, on reload it" should "have all previously saved data" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val shortUrlsJson = shortUrls.asJson
        .toString()
        .filterNot(_.isWhitespace)

      val backendStub = buildBackend(api.getAllShortUrls)
      populate.run(appCtx).unsafeRunSync()
      val response =
        basicRequest.header("Authorization", s"Basic $testCredentials").get(uri"$shortUrlsPath").send(backendStub)
      response.map(_.body.value shouldBe shortUrlsJson).run(appCtx).unsafeRunSync()
      backendStub.close().run(appCtx).unsafeRunSync()

      val newBackendStub = buildBackend(api.getAllShortUrls)
      val newResponse =
        basicRequest.header("Authorization", s"Basic $testCredentials").get(uri"$shortUrlsPath").send(newBackendStub)
      newResponse.map(_.body.value shouldBe shortUrlsJson).run(appCtx).unsafeRunSync()
    })
  }

  "GET /api/v1/users/:id" should "return user" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.getUser)
      val response = basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .get(uri"${userPath(testUser.id.toString)}")
        .send(backendStub)
      val userJson = userToReadDto(testUser).asJson.toString().filterNot(_.isWhitespace)
      response.map(_.body.value shouldBe userJson).run(appCtx).unsafeRunSync()
    })
  }

  it should "return error if user not found" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.getUser)
      val testId      = "00000000-0000-0000-0000-000000000000"
      val response =
        basicRequest.header("Authorization", s"Basic $testCredentials").get(uri"${userPath(testId)}").send(backendStub)
      response.map(_.body shouldBe Left("User not found")).run(appCtx).unsafeRunSync()
    })
  }

  "POST /api/v1/users" should "create user" in {
    withContainers(new Context(_) {
      val backendStub  = buildBackend(api.createUser)
      val creatingUser = UserWriteDto("newUser", "newPassword", List("Admin", "SuperUser"))
      val payload      = creatingUser.asJson.toString().filterNot(_.isWhitespace)
      val response = basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .post(uri"${usersPath}")
        .body(payload)
        .send(backendStub)

      response
        .map(resp =>
          decode[UserReadDto](resp.body.value).map { userReadDto =>
            userReadDto.login shouldBe creatingUser.login
            userReadDto.password shouldBe creatingUser.password
            userReadDto.roles shouldBe creatingUser.roles
          }
        )
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error if payload is corrupted" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.createUser)
      val payload     = s"{\"login\": \"test\", \"passwod\": \"test\"}"
      val response = basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .post(uri"${usersPath}")
        .body(payload)
        .send(backendStub)
      response
        .map(
          _.body shouldBe Left(
            "Invalid value for: body (Missing required field at 'password', Missing required field at 'roles')"
          )
        )
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  "PUT /api/v1/users/:id" should "change user" in {
    withContainers(new Context(_) {
      val backendStub  = buildBackend(api.updateUser)
      val updatingUser = UserUpdateDto("user", "newPass", List("SuperUser"))
      val payload      = updatingUser.asJson.toString().filterNot(_.isWhitespace)
      (basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .put(uri"${userPath(testUser.id.toString)}")
        .body(payload)
        .send(backendStub) >>
        userDao.get(testUser.id)).run(appCtx).unsafeRunSync() match {
        case Some(user) => {
          user.login shouldBe updatingUser.login
          BCrypt.checkpw(updatingUser.password, user.password) shouldBe true
          user.roles.map(_.entryName) shouldBe updatingUser.roles
        }
        case _ => fail("User not found")
      }
    })
  }

  it should "return error if payload is corrupted" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.updateUser)
      val payload     = s"{\"login\": \"test\", \"passwod\": \"test\"}"
      val response = basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .put(uri"${userPath(testUser.id.toString)}")
        .body(payload)
        .send(backendStub)
      response
        .map(
          _.body shouldBe Left(
            "Invalid value for: body (Missing required field at 'password', Missing required field at 'roles')"
          )
        )
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  "DELETE /api/v1/users/:id" should "delete user" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.removeUser)
      (basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .delete(uri"${userPath(testUser.id.toString)}")
        .send(backendStub) >>
        userDao.get(testUser.id)).run(appCtx).unsafeRunSync() match {
        case Some(_) => {
          fail("User must be deleted")
        }
        case _ => succeed
      }
    })
  }
}

package dev.yanovsky.myproject

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import cats.implicits._
import com.dimafeng.testcontainers.{ContainerDef, PostgreSQLContainer}
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import dev.yanovsky.myproject.components.Metrics
import dev.yanovsky.myproject.configs.AppConfig
import dev.yanovsky.myproject.core.{AppCtx, SimpleRolePolicy}
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import dev.yanovsky.myproject.daos.{ShortUrlDaoImpl, UserDaoImpl}
import dev.yanovsky.myproject.http.Api
import dev.yanovsky.myproject.models.ShortUrl
import doobie.implicits._
import doobie.util.transactor.Transactor
import io.prometheus.client.CollectorRegistry
import org.flywaydb.core.Flyway
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.testcontainers.utility.DockerImageName
import pureconfig._
import pureconfig.module.catseffect.syntax._
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{UriContext, basicRequest}
import sttp.tapir.integ.cats.CatsMonadError
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.metrics.prometheus.PrometheusMetrics
import sttp.tapir.server.stub.TapirStubInterpreter
import java.util.UUID
class BasicAuthSpec extends AnyFlatSpec with Matchers with EitherValues with TestContainerForAll {
  override val containerDef = PostgreSQLContainer.Def(
    dockerImageName = DockerImageName.parse("postgres:15.1"),
    databaseName = "testcontainer-scala",
    username = "scala",
    password = "scala"
  )

  val collectorRegistry = CollectorRegistry.defaultRegistry
  val tapirMetrics      = PrometheusMetrics.default[WithMyContext]("BasicAuthSpecMetrics", collectorRegistry)

  class Context(container: PostgreSQLContainer) {
    Class.forName(container.driverClassName)

    val flywayConfig = Flyway.configure
      .locations("migrations/")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()
    val transactor =
      Transactor
        .fromDriverManager[WithMyContext](
          container.driverClassName,
          container.jdbcUrl,
          container.username,
          container.password
        )
    val interpreter = TapirStubInterpreter(SttpBackendStub(new CatsMonadError[WithMyContext]()))
    val config      = ConfigSource.default.loadF[IO, AppConfig].unsafeRunSync()
    val shortUrlDao = new ShortUrlDaoImpl(transactor, config.dao)
    val userDao     = new UserDaoImpl[WithMyContext](transactor)
    val testUserId: UUID = userDao
      .get("admin")
      .map(_.map(_.id))
      .run(appCtx)
      .unsafeRunSync()
      .getOrElse(throw new RuntimeException("Migrations fail"))
    val policy = new SimpleRolePolicy[WithMyContext]

    val metrix = Metrics(collectorRegistry, tapirMetrics)
    val api    = new Api(shortUrlDao, userDao, policy, metrix)

    val shortUrls = Set[ShortUrl](
      ShortUrl("1111111111", "https://example.com/1"),
      ShortUrl("2222222222", "https://example.com/2")
    )

    def populate = {
      val insertShortUrlsQueries = shortUrls.map { url =>
        sql"INSERT INTO short_urls (id, source_url) VALUES (${url.id}, ${url.sourceUrl})".update.run
      }
      val insertUserShortUrlsQueries = shortUrls.map { url =>
        sql"INSERT INTO user_short_urls (user_id, short_url_id) VALUES (${testUserId.toString}::uuid, ${url.id})".update.run
      }
      val combinedQuery = (insertShortUrlsQueries ++ insertUserShortUrlsQueries).toList.traverse(_.void)
      combinedQuery.transact(transactor)
    }

    val testCredentials = java.util.Base64.getEncoder.encodeToString("justForTest:justForTest".getBytes("UTF-8"))

    val appCtx = AppCtx("BasicAuthSpec")

    def truncate = sql"TRUNCATE TABLE short_urls CASCADE".update.run.transact(transactor).run(appCtx)

    def buildBackend[A, U, I, E, O, R](serverEndpoint: ServerEndpoint.Full[A, U, I, E, O, R, WithMyContext]) =
      interpreter.whenServerEndpoint(serverEndpoint).thenRunLogic().backend()

    private val baseUrl   = s"http://localhost:${config.port}"
    private val apiV1Path = s"$baseUrl/api/v1"
    val helloWorldPath    = s"$apiV1Path/hello"
    val shortUrlsPath     = s"$apiV1Path/shorturls"
    val usersPath         = s"$apiV1Path/users"

    def shortUrlPath(id: String) = s"$shortUrlsPath/$id"
    def userPath(id: String)     = s"$usersPath/$id"

    def redirectPath(id: String) = s"/r/$id"
  }

  "GET /api/v1/shorturls" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getAllShortUrls)

      val response = populate >> basicRequest.get(uri"$shortUrlsPath").send(backendStub)
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getAllShortUrls)

      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .get(uri"$shortUrlsPath")
        .send(backendStub)
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "GET /api/v1/shorturls/:id" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getShortUrl)
      val testId      = "1111111111"
      val response    = populate >> basicRequest.get(uri"${shortUrlPath(testId)}").send(backendStub)
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getShortUrl)
      val testId      = "1111111111"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .get(uri"${shortUrlPath(testId)}")
        .send(backendStub)
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "POST /api/v1/shorturls" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.createShortUrl)
      val testSourceUrl = "https://google.com"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response      = populate >> basicRequest.post(uri"$shortUrlsPath").body(payload).send(backendStub)
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.createShortUrl)
      val testSourceUrl = "https://google.com"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response = populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .post(uri"$shortUrlsPath")
        .body(payload)
        .send(backendStub)
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "PUT /api/v1/shorturls/:id" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.updateShortUrl)
      val testId        = "1111111111"
      val testSourceUrl = "http://test.com"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response      = (populate >> basicRequest.put(uri"${shortUrlPath(testId)}").body(payload).send(backendStub))
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub   = buildBackend(api.updateShortUrl)
      val testId        = "1111111111"
      val testSourceUrl = "http://test.com"
      val payload       = s"{\"sourceUrl\": \"$testSourceUrl\"}"
      val response = (populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .put(uri"${shortUrlPath(testId)}")
        .body(payload)
        .send(backendStub))
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "DELETE /api/v1/shorturls/:id" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.removeShortUrl)
      val testId      = "1111111111"
      val response    = (populate >> basicRequest.delete(uri"${shortUrlPath(testId)}").send(backendStub))
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.removeShortUrl)
      val testId      = "1111111111"
      val response = (populate >> basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .delete(uri"${shortUrlPath(testId)}")
        .send(backendStub))
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "GET /api/v1/users/:id" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getUser)
      val testId      = "00000000-0000-0000-0000-000000000000"
      val response    = basicRequest.get(uri"${userPath(testId)}").send(backendStub)
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.getUser)
      val testId      = "00000000-0000-0000-0000-000000000000"
      val response =
        basicRequest.header("Authorization", s"Basic $testCredentials").get(uri"${userPath(testId)}").send(backendStub)
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "POST /api/v1/users" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.createUser)
      val response    = basicRequest.post(uri"${usersPath}").send(backendStub)
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.createUser)
      val response =
        basicRequest.header("Authorization", s"Basic $testCredentials").post(uri"${usersPath}").send(backendStub)
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "PUT /api/v1/users/:id" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.updateUser)
      val testId      = "00000000-0000-0000-0000-000000000000"
      val response    = basicRequest.put(uri"${userPath(testId)}").send(backendStub)
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.updateUser)
      val testId      = "00000000-0000-0000-0000-000000000000"
      val response =
        basicRequest.header("Authorization", s"Basic $testCredentials").put(uri"${userPath(testId)}").send(backendStub)
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }

  "DELETE /api/v1/users/:id" should "return error: Invalid value for: header Authorization (missing)" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.removeUser)
      val testId      = "00000000-0000-0000-0000-000000000000"
      val response    = basicRequest.delete(uri"${userPath(testId)}").send(backendStub)
      response
        .map(_.body shouldBe Left("Invalid value for: header Authorization (missing)"))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return error: User not found or password wrong" in {
    withContainers(new Context(_) {
      truncate.unsafeRunSync()

      val backendStub = buildBackend(api.removeUser)
      val testId      = "00000000-0000-0000-0000-000000000000"
      val response = basicRequest
        .header("Authorization", s"Basic $testCredentials")
        .delete(uri"${userPath(testId)}")
        .send(backendStub)
      response.map(_.body shouldBe Left("User not found or password wrong")).run(appCtx).unsafeRunSync()
    })
  }
}

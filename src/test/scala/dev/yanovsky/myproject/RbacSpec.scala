package dev.yanovsky.myproject

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import com.dimafeng.testcontainers.{ContainerDef, PostgreSQLContainer}
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import dev.yanovsky.myproject.components.Metrics
import dev.yanovsky.myproject.configs.AppConfig
import dev.yanovsky.myproject.core.{AppCtx, SimpleRolePolicy}
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import dev.yanovsky.myproject.daos.{ShortUrlDaoImpl, UserDaoImpl}
import dev.yanovsky.myproject.http.Api
import doobie.util.transactor.Transactor
import io.prometheus.client.CollectorRegistry
import org.flywaydb.core.Flyway
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.testcontainers.utility.DockerImageName
import pureconfig._
import pureconfig.module.catseffect.syntax._
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{UriContext, basicRequest}
import sttp.tapir.integ.cats.CatsMonadError
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.metrics.prometheus.PrometheusMetrics
import sttp.tapir.server.stub.TapirStubInterpreter
import java.util.UUID

class RbacSpec extends AnyFlatSpec with Matchers with EitherValues with TestContainerForAll {
  override val containerDef = PostgreSQLContainer.Def(
    dockerImageName = DockerImageName.parse("postgres:15.1"),
    databaseName = "testcontainer-scala",
    username = "scala",
    password = "scala"
  )
  val collectorRegistry = CollectorRegistry.defaultRegistry
  val tapirMetrics      = PrometheusMetrics.default[WithMyContext]("RbacSpecMetrics", collectorRegistry)

  class Context(container: PostgreSQLContainer) {
    Class.forName(container.driverClassName)

    val flywayConfig = Flyway.configure
      .locations("migrations/")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()
    val transactor =
      Transactor
        .fromDriverManager[WithMyContext](
          container.driverClassName,
          container.jdbcUrl,
          container.username,
          container.password
        )
    val interpreter = TapirStubInterpreter(SttpBackendStub(new CatsMonadError[WithMyContext]()))
    val config      = ConfigSource.default.loadF[IO, AppConfig].unsafeRunSync()
    val shortUrlDao = new ShortUrlDaoImpl(transactor, config.dao)
    val userDao     = new UserDaoImpl[WithMyContext](transactor)
    val policy      = new SimpleRolePolicy[WithMyContext]

    val metrix = Metrics(collectorRegistry, tapirMetrics)
    val api    = new Api(shortUrlDao, userDao, policy, metrix)

    val adminCredentials     = java.util.Base64.getEncoder.encodeToString("admin:admin".getBytes("UTF-8"))
    val superUserCredentials = java.util.Base64.getEncoder.encodeToString("superuser:superuser".getBytes("UTF-8"))
    val userCredentials      = java.util.Base64.getEncoder.encodeToString("user:user".getBytes("UTF-8"))

    val appCtx = AppCtx("RbacSpec")

    def buildBackend[A, U, I, E, O, R](serverEndpoint: ServerEndpoint.Full[A, U, I, E, O, R, WithMyContext]) =
      interpreter.whenServerEndpoint(serverEndpoint).thenRunLogic().backend()

    private val baseUrl   = s"http://localhost:${config.port}"
    private val apiV1Path = s"$baseUrl/api/v1"
    val shortUrlsPath     = s"$apiV1Path/shorturls"
    val usersPath         = s"$apiV1Path/users"

    def shortUrlPath(id: String) = s"$shortUrlsPath/$id"
    def userPath(id: String)     = s"$usersPath/$id"
  }

  "user with role User" should "not be able to create more than 10 short urls" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.createShortUrl)
      for (i <- 0 to 10) {
        val payload = s"{\"sourceUrl\": \"https://google.com/\"}"
        val response = basicRequest
          .header("Authorization", s"Basic $userCredentials")
          .post(uri"$shortUrlsPath")
          .body(payload)
          .send(backendStub)
        if (i < 10) {
          response.map(_.body.isRight shouldBe true).run(appCtx).unsafeRunSync()
        } else {
          response.map(_.body.isLeft shouldBe true).run(appCtx).unsafeRunSync()
        }
      }
    })
  }

  "user with role SuperUser" should "be able to create an infinite number of short urls" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.createShortUrl)
      for (i <- 0 to 20) {
        val payload = s"{\"sourceUrl\": \"https://google.com/\"}"
        val response = basicRequest
          .header("Authorization", s"Basic $superUserCredentials")
          .post(uri"$shortUrlsPath")
          .body(payload)
          .send(backendStub)
        response.map(_.body.isRight shouldBe true).run(appCtx).unsafeRunSync()
      }
    })
  }

  "only user with Admin role" should "be able to interact users endpoints" in {
    withContainers(new Context(_) {
      val backendStub = buildBackend(api.getUser)
      val testId      = UUID.fromString("00000000-0000-0000-0000-000000000000")
      val adminResponse = basicRequest
        .header("Authorization", s"Basic $adminCredentials")
        .get(uri"${userPath(testId.toString)}")
        .send(backendStub)
      adminResponse.map(_.body shouldBe Left("User not found")).run(appCtx).unsafeRunSync()

      val superUserResponse = basicRequest
        .header("Authorization", s"Basic $superUserCredentials")
        .get(uri"${userPath(testId.toString)}")
        .send(backendStub)
      superUserResponse.map(_.body shouldBe Left("Access denied")).run(appCtx).unsafeRunSync()

      val userResponse = basicRequest
        .header("Authorization", s"Basic $userCredentials")
        .get(uri"${userPath(testId.toString)}")
        .send(backendStub)
      userResponse.map(_.body shouldBe Left("Access denied")).run(appCtx).unsafeRunSync()
    })
  }
}

package dev.yanovsky.myproject

import cats.effect.IO
import cats.implicits._
import cats.effect.unsafe.implicits.global
import com.dimafeng.testcontainers.PostgreSQLContainer
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import dev.yanovsky.myproject.configs.AppConfig
import dev.yanovsky.myproject.core.AppCtx
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import doobie.implicits._
import doobie.util.transactor.Transactor
import dev.yanovsky.myproject.models.ShortUrl
import dev.yanovsky.myproject.daos.{ShortUrlDaoImpl, UserDaoImpl}
import org.flywaydb.core.Flyway
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.testcontainers.utility.DockerImageName
import pureconfig._
import pureconfig.module.catseffect.syntax._
import sttp.client3.testing.SttpBackendStub
import sttp.tapir.integ.cats.CatsMonadError
import sttp.tapir.server.stub.TapirStubInterpreter

import java.util.UUID

class ShortUrlDaoSpec extends AnyFlatSpec with Matchers with EitherValues with TestContainerForAll {
  override val containerDef = PostgreSQLContainer.Def(
    dockerImageName = DockerImageName.parse("postgres:15.1"),
    databaseName = "testcontainer-scala",
    username = "scala",
    password = "scala"
  )

  class Context(container: PostgreSQLContainer) {
    Class.forName(container.driverClassName)

    val flywayConfig = Flyway.configure
      .locations("migrations/")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()

    val transactor =
      Transactor
        .fromDriverManager[WithMyContext](
          container.driverClassName,
          container.jdbcUrl,
          container.username,
          container.password
        )
    val interpreter = TapirStubInterpreter(SttpBackendStub(new CatsMonadError[WithMyContext]()))
    val config      = ConfigSource.default.loadF[IO, AppConfig].unsafeRunSync()
    val shortUrlDao = new ShortUrlDaoImpl(transactor, config.dao)
    val userDao     = new UserDaoImpl[WithMyContext](transactor)

    val shortUrls = Set[ShortUrl](
      ShortUrl("1111111111", "https://example.com/1"),
      ShortUrl("2222222222", "https://example.com/2")
    )
    val testUserId: UUID = userDao
      .get("admin")
      .map(_.map(_.id))
      .run(appCtx)
      .unsafeRunSync()
      .getOrElse(throw new RuntimeException("Migrations fail"))

    def populate = {
      val insertQueries = shortUrls.map { url =>
        sql"INSERT INTO short_urls (id, source_url) VALUES (${url.id}, ${url.sourceUrl})".update.run
      }
      val combinedQuery = insertQueries.toList.traverse(_.void)
      combinedQuery.transact(transactor)
    }

    def truncate = sql"TRUNCATE TABLE short_urls CASCADE".update.run.transact(transactor)
    val appCtx   = AppCtx("ShortUrlDaoSpec")
  }

  "getAll method" should "return empty set if there are no short urls" in {
    withContainers(new Context(_) {
      (truncate >>
        shortUrlDao.getAll().map(_ shouldBe Set())).run(appCtx).unsafeRunSync()
    })
  }

  it should "return set of short urls" in {
    withContainers(new Context(_) {
      (truncate >> populate >>
        shortUrlDao.getAll().map(_ shouldBe shortUrls)).run(appCtx).unsafeRunSync()
    })
  }

  "getById method" should "return short url" in {
    withContainers(new Context(_) {
      (truncate >> populate >>
        shortUrlDao.getById(shortUrls.head.id).map(_ shouldBe Right(shortUrls.head))).run(appCtx).unsafeRunSync()
    })
  }

  it should "return Left if there are no short url with presented id" in {
    withContainers(new Context(_) {
      val testId = "testId"
      (truncate >> populate >>
        shortUrlDao.getById(testId).map(_ shouldBe Left(s"Short url with id = $testId not found.")))
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  "create method" should "add short url to db" in {
    withContainers(new Context(_) {
      val result = (truncate >> shortUrlDao.create(testUserId, shortUrls.head)).run(appCtx).unsafeRunSync()
      result match {
        case Right(ShortUrl(_, sourceUrl)) =>
          sourceUrl shouldBe shortUrls.head.sourceUrl
        case Left(error) =>
          fail(s"Expected Right, but got Left with error: $error")
      }
    })
  }

  "update method" should "update existing short url" in {
    withContainers(new Context(_) {
      val updatedShortUrl = ShortUrl(shortUrls.head.id, "http://newurl.com/")
      val result = (truncate >> populate >>
        shortUrlDao.update(updatedShortUrl).map(_ shouldBe Right(())) >>
        shortUrlDao.getById(updatedShortUrl.id)).run(appCtx).unsafeRunSync()
      result match {
        case Right(ShortUrl(id, sourceUrl)) =>
          id shouldBe updatedShortUrl.id
          sourceUrl shouldBe updatedShortUrl.sourceUrl
        case Left(error) =>
          fail(s"Expected Right, but got Left with error: $error")
      }
    })
  }

  it should "return Left if there are no short url with presented id" in {
    withContainers(new Context(_) {
      val testId          = "testId"
      val updatedShortUrl = ShortUrl(testId, shortUrls.head.sourceUrl)
      (truncate >> populate >>
        shortUrlDao
          .update(updatedShortUrl)
          .map(_ shouldBe Left(s"Short url with id = ${updatedShortUrl.id} not found."))).run(appCtx).unsafeRunSync()
    })
  }

  "remove method" should "remove existing short url" in {
    withContainers(new Context(_) {
      (truncate >> populate >>
        shortUrlDao.remove(shortUrls.head.id).map(_ shouldBe Right(())) >>
        shortUrlDao
          .getById(shortUrls.head.id)
          .map(_ shouldBe Left(s"Short url with id = ${shortUrls.head.id} not found."))).run(appCtx).unsafeRunSync()
    })
  }

  it should "return Left if there are no short url with presented id" in {
    withContainers(new Context(_) {
      val testId = "testId"
      (truncate >> populate >>
        shortUrlDao.remove(testId).map(_ shouldBe Left(s"Short url with id = $testId not found.")))
        .run(appCtx)
        .unsafeRunSync()
    })
  }
}

package dev.yanovsky.myproject

import cats.effect.unsafe.implicits.global
import com.dimafeng.testcontainers.{ContainerDef, PostgreSQLContainer}
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import dev.yanovsky.myproject.core.AppCtx
import dev.yanovsky.myproject.core.AppCtx.WithMyContext
import doobie.util.transactor.Transactor
import dev.yanovsky.myproject.models.{Role, ShortUrl, User}
import dev.yanovsky.myproject.daos.{ShortUrlDaoImpl, UserDaoImpl}
import org.flywaydb.core.Flyway
import org.mindrot.jbcrypt.BCrypt
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.testcontainers.utility.DockerImageName
import sttp.client3.testing.SttpBackendStub
import sttp.tapir.integ.cats.CatsMonadError
import sttp.tapir.server.stub.TapirStubInterpreter

import java.util.UUID
class UserDaoSpec extends AnyFlatSpec with Matchers with EitherValues with TestContainerForAll {
  override val containerDef = PostgreSQLContainer.Def(
    dockerImageName = DockerImageName.parse("postgres:15.1"),
    databaseName = "testcontainer-scala",
    username = "scala",
    password = "scala"
  )

  class Context(container: PostgreSQLContainer) {
    Class.forName(container.driverClassName)

    val flywayConfig = Flyway.configure
      .locations("migrations/")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()

    val transactor =
      Transactor
        .fromDriverManager[WithMyContext](
          container.driverClassName,
          container.jdbcUrl,
          container.username,
          container.password
        )
    val interpreter = TapirStubInterpreter(SttpBackendStub(new CatsMonadError[WithMyContext]()))
    val userDao     = new UserDaoImpl[WithMyContext](transactor)
    val appCtx      = AppCtx("ShortUrlDaoSpec")

    val testUser = User(UUID.randomUUID(), "user", "user", List(Role.User), List())
  }

  "get by login method" should "return Some(user) if exists" in {
    withContainers(new Context(_) {
      userDao
        .get(testUser.login)
        .map {
          case Some(user) => {
            user.login shouldBe testUser.login
            BCrypt.checkpw(testUser.password, user.password) shouldBe true
            user.roles shouldBe testUser.roles
            user.shortUrls shouldBe testUser.shortUrls
          }
          case _ => fail("User not found")
        }
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  it should "return None if user not exists" in {
    withContainers(new Context(_) {
      userDao
        .get("")
        .map {
          case Some(_) => fail("Should be None")
          case _       => succeed
        }
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  "get by id method" should "return Some(user) if exists" in {
    withContainers(new Context(_) {
      (for {
        testUserId: Option[UUID] <- userDao.get(testUser.login).map(_.map(_.id))
        user: Option[User]       <- userDao.get(testUserId.get).map(_.map(u => u))
      } yield user.map {
        case user => {
          user.login shouldBe testUser.login
          BCrypt.checkpw(testUser.password, user.password) shouldBe true
          user.roles shouldBe testUser.roles
          user.shortUrls shouldBe testUser.shortUrls
        }
        case _ => fail("User not found")
      }).run(appCtx).unsafeRunSync()
    })
  }

  it should "return None if user not exists" in {
    withContainers(new Context(_) {
      val testId: UUID = UUID.fromString("00000000-0000-0000-0000-000000000000")
      userDao
        .get(testId)
        .map {
          case Some(_) => fail("Should be None")
          case _       => succeed
        }
        .run(appCtx)
        .unsafeRunSync()
    })
  }

  "create method" should "add user to db and return generated id" in {
    withContainers(new Context(_) {
      val creatingUser = User(UUID.randomUUID(), "newUser", "newPassword", List(Role.SuperUser), List())
      (for {
        creatingUserId: UUID      <- userDao.create(creatingUser)
        createdUser: Option[User] <- userDao.get(creatingUserId)
      } yield createdUser.map {
        case user: User => {
          user.login shouldBe creatingUser.login
          BCrypt.checkpw(creatingUser.password, user.password) shouldBe true
          user.roles shouldBe creatingUser.roles
          user.shortUrls shouldBe creatingUser.shortUrls
        }
        case _ => fail("User not created")
      }).run(appCtx).unsafeRunSync()
    })
  }

  "put method" should "change user" in {
    withContainers(new Context(_) {
      val changingUser = User(UUID.randomUUID(), "user", "newPassword", List(Role.SuperUser), List())
      (for {
        testUserId: Option[UUID]  <- userDao.get(testUser.login).map(_.map(_.id))
        _                         <- userDao.put(testUserId.get, changingUser)
        changedUser: Option[User] <- userDao.get(testUserId.get)
      } yield changedUser.map {
        case user: User => {
          user.login shouldBe changingUser.login
          BCrypt.checkpw(changingUser.password, user.password) shouldBe true
          user.roles shouldBe changingUser.roles
          user.shortUrls shouldBe changingUser.shortUrls
        }
        case _ => fail("User not found")
      }).run(appCtx).unsafeRunSync()
    })
  }

  "delete method" should "delete user" in {
    withContainers(new Context(_) {
      (for {
        testUserId: Option[UUID]  <- userDao.get(testUser.login).map(_.map(_.id))
        _                         <- userDao.delete(testUserId.get)
        deletedUser: Option[User] <- userDao.get(testUserId.get)
      } yield deletedUser.map {
        case user: User => {
          fail("User should be deleted")
        }
        case _ => succeed
      }).run(appCtx).unsafeRunSync()
    })
  }
}
